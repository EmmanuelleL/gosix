let size = 100;
let canvasWidth=800;
let canvasL = Math.floor(canvasWidth/(Math.sqrt(3) * size));
let canvasHeight=600;
let canvasH= Math.floor(canvasHeight/(size*2*3/4));
let hexagones = [];
var player = 1;

class Point {
	constructor(q, r) {
			this.q = q;
		    this.r = r;
  	}

  	toHex(){
  		var x = this.q;
	    var y = this.r;
	    var z = 0 -this.q -this.r;
	    var hex = new Hex(x, y);
	    return hex;
  	}

}

class Hex {

  	toPoint(){
	    var q = this.x;
	    var r = this.y;
	    var center = new Point(q, r);
	    return center;
	}
	
	constructor(x, y) {
		
		if (y%2 != 0){
			this.x = x*((Math.sqrt(3) * size));
		    this.y = size*2*3/4*y;
		    this.z = 0-this.x-this.y;
		    this.center = this.toPoint();
		}else{
			this.y = size*2*3/4*y;
			this.x = x*((Math.sqrt(3) * size))+((Math.sqrt(3) * size)/2);
			this.z = 0-this.x-this.y;
		    this.center = this.toPoint();
		}
		
  	}

	pointy_hex_corner(center, size, i){
		    var angle_deg = 60 * i - 30;
		    var angle_rad = Math.PI / 180 * angle_deg;
		    var angle_i = new Point(center.x + size * Math.cos(angle_rad), center.y + size * Math.sin(angle_rad));
		    return angle_i;
			};

	creat_hexa (size){
		for (var i = 0; i < 6; i++) {
			this["point"+i] = this.pointy_hex_corner(this, size, i);
		}
	}

	
};

class Afficher {
	
	getCanvas(id){
	this.canvas = document.getElementById(id); 
	this.canvas.addEventListener('mousedown', getCorner, false);
		if (this.canvas.getContext){    
		    this.context = this.canvas.getContext('2d'); 
		}

		this.context.translate(canvasWidth/2,canvasHeight/2);
	}

	drawLine(a, b){
		this.context.beginPath();
		this.context.moveTo(a.q, a.r);
		this.context.lineTo(b.q, b.r);
		this.context.stroke();
	}

	drawHexa(hexa, color){		
		this.context.beginPath();
		this.context.moveTo(hexa["point0"].q, hexa["point0"].r);
		for (var i = 0; i < 6; i++) {
			this.context.lineTo(hexa["point"+i].q, hexa["point"+i].r);
			this.context.fillStyle =color;
			this.context.strokeStyle = "#000";
       		this.context.stroke();
			this.context.fill();
			
			
		}
		for (var i = 0; i < 6; i++) {
			this.context.beginPath();
			this.context.arc(hexa["point"+i].q, hexa["point"+i].r,10, 0, 2*Math.PI)
			this.context.fillStyle ="white";
			this.context.stroke();
			this.context.fill();
		}
	}


}
var moncanevas;
nb= 0;
 moncanevas = new Afficher();

	moncanevas.getCanvas("moncanevas");

	

function creat_grid(hauteur, largeur){
	

	for (var i = 0; i < largeur; i++) {
		for (var j = 0; j < hauteur; j++) {
			var hexa= new Hex(i, j);
			hexagones.push(hexa);
			hexa.creat_hexa(size);
			moncanevas.drawHexa(hexa, "pink");
		}
	}

}

function gosix(){
	var axial_directions = [
    [1, 0], [1, -1], [ 0, -1], 
    [-1, 0], [1, 1], [ 0, 1]
	];

	var hexa= new Hex(0,0 );
		hexagones.push(hexa);
		hexa.creat_hexa(size);
		moncanevas.drawHexa(hexa, "blue");

	for (direction in axial_directions){
		var hexa= new Hex(axial_directions[direction][0], axial_directions[direction][1] );
		hexagones.push(hexa);
		hexa.creat_hexa(size);
		moncanevas.drawHexa(hexa, "blue");
	}

}

function deleteHexasPoints(hexagone){
	h =0;
	for (var j= 0; j<7; j++){
		for (var i = 0; i < 6; i++) {
			var k = 0;
			match = false;
			for (var k = 0; k < 6; k++){
				h++
				if (hexagones[j]["point"+ i].q< (hexagone["point"+ k].q+.3)&& hexagones[j]["point"+ i].q> (hexagone["point"+ k].q-.3) && hexagones[j]["point"+ i].r <(hexagone["point"+ k].r+.3)&&hexagones[j]["point"+ i].r >(hexagone["point"+ k].r-.3)){
					delete(hexagones[j]["point"+ i].take);
				}
			
			
			}

		}
	}
}


function takeHexa(hexagone, player){
	//inteval evite que le rond se recolorie
	window.setTimeout(function(){
		hexagone.take = player;
		if (player === 1) {
			moncanevas.drawHexa(hexagone, "pink");
		}else{
			moncanevas.drawHexa(hexagone, "yellow");
		}
		deleteHexasPoints(hexagone);
	}, 50);
}

function verifyHexa(hexagone, player){
	var point=0;
	var i=0
	while(point<=4 && i<6){
		if (hexagone["point"+ i].take === player){
			point++;
		}
		i++;
	}
	
	if (point >= 4) {
		takeHexa(hexagone, player);
		return true;
	}
	return false;
}

function getCorner(coords){
	var played = false;
	for (var j= 0; j<7; j++){
		for (var i = 0; i < 6; i++) {
			var mouseX = coords.x- 400 ;
			var mouseY = coords.y - 300;
			var angleX = hexagones[j]["point"+ i].q;
			var angleY = hexagones[j]["point"+ i].r;
			if ((angleX - 10 <= mouseX && mouseX<= angleX +10) && (angleY - 10 <=mouseY && mouseY<= angleY +10)){
				if(typeof(hexagones[j]["point"+ i].take)=== "undefined"){
					moncanevas.context.beginPath();
					moncanevas.context.arc(angleX, angleY, 10, 0, 2*Math.PI);
					if (player === 1) {
						moncanevas.context.fillStyle ="pink";
					}else{
						moncanevas.context.fillStyle ="yellow";
					}
					moncanevas.context.strokeStyle = "#000";
		       		moncanevas.context.stroke();
					moncanevas.context.fill();

					hexagones[j]["point"+ i].take = player;
					played = true;
					if(typeof(hexagones[j].take)=== "undefined"){
					verifyHexa(hexagones[j], player);
					}
				}
			}
		}
	}
	if (played === true) {
		if (player === 1) {
			player = 2;
		}else{
			player = 1;
		}
	}
}

function getCoords(el,event) {
  var ox = -el.offsetLeft,
  oy = -el.offsetTop;
  while(el=el.offsetParent){
    ox += el.scrollLeft - el.offsetLeft;
    oy += el.scrollTop - el.offsetTop;
  }
  return {x:event.clientX + ox , y:event.clientY + oy};
}
 
 
 
moncanevas.canvas.onmouseup = function(e) {
    	var coords = getCoords(this,e);
  		getCorner(coords);
};